# Проектная работа по курсу OTUS «DevOps: практики и инструменты»

## Что есть

- Приложение, предоставленное авторами курса.
- Минимальный Compose-файл для локального запуска.
- Управляемый кластер Kubernetes на Yandex Cloud, развёртываемый Terraform.
- GitLab on Kubernetes, устанавливаемый Helm.
- GitLab CI/CD-пайплайны для сборки, тестирования и выкатки приложения в
  кластер.
- Prometheus stack для сбора метрик и алертинга.
- Все чарты настраиваются через values и манифесты, сторонние чарты в
  репозитории отсутствуют.

## Требования

- Terraform
- kubectl
- Helm
- CLI Yandex.Cloud
- Git

> CLI Yandex.Cloud не отвечает критериям свободного или открытого программного
> обеспечения

> При создании HCL-шаблонов применялся Terraform v1.1.7

> При создании чартов применялся Helm v3.8.0

## Запуск

1. [Создайте файл авторизованных
   ключей](https://cloud.yandex.ru/docs/container-registry/operations/authentication#sa-json)
   для вашего аккаунта Yandex Cloud.
1. Укажите путь к файлу ключей и другие желаемые опции в файле
   `terraform/terraform.tfvars`. См. файл
   [terraform/terraform.tfvars.example](terraform/terraform.tfvars.example).
1. Инициализируйте Terraform командой `terraform -chdir=terraform init`.
1. Создайте кластер Kubernetes командой `terraform -chdir=terraform apply
   -auto-approve`, по завершении исполнения в стандартный вывод будет
   напечатана команда по настройке kubectl
1. Настройте kubectl командой, полученной на прошлом шаге (__требуется
   проприетарная утилита yc__)
1. Установите NGINX Ingress Controller командами:
```shell
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install ingress-nginx nginx-stable/nginx-ingress
```
1. Добавьте репозиторий helm-чартов GitLab командой `helm repo add gitlab
   https://charts.gitlab.io`.
1. Отредактируйте [gitlab-chart-values.yaml](gitlab-chart-values.yaml) согласно
   имеющимся в вашем распоряжении доменным именем и почтовым ящиком.
1. Установите на кластер GitLab командой `helm install --values
   gitlab-chart-values.yaml gitlab gitlab/gitlab`. После установки пароль
   пользователя `root` может быть получен командой `kubectl get secret
   gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64
   --decode`.
1. Обновите ресурсные записи своего домена, как минимум поддомены `kas`,
   `gitlab`, `minio`, `registry` должны указывать на адреса соответствующих
   ингрессов. Список ингрессов можно получить командой `kubectl get ingress`.
   После успешного обновления записей GitLab получит сертификаты для поддоменов
   и будет работать в штатном режиме. Обновление в системе DNS может занять
   значительное время.
1. Делигируйте административные права служебной УЗ GitLab командой `kubectl
   create clusterrolebinding --clusterrole=cluster-admin
   --group=system:serviceaccounts gitlab-cluster-admin`
1. Войдите на новый инстанс GitLab с логином `root` и паролем, полученным на
   одном из предыдущих шагов. Опционально создайте пользователя и группу для
   проекта.
1. Создайте группу (пространство) для репозиториев.
1. Создайте репозитории search_engine_ui, search_engine_crawler. Отправьте в
   них код соответствующих микросервисов, а так же закомитьте файлы из
   одноимённых поддиректорий данного репозитория.
1. Создайте репозиторий search_engine_deploy и закомитьте в него файлы из
   одноимённой поддиректории данного репозитория.
1. В настройках репозитория search_engine_deploy создайте триггер с произвольным
   названием. Он потребуется для запуска пайплайна из других пайплайнов.
1. В настройках группы проектов создайте незащищённую маскированную переменную
   `DEPLOY_TRIGGER_TOKEN` и незащищённую переменную `DEPLOY_TRIGGER_PROJECT`
   (целое число как в ссылке) согласно данным, предоставленным GitLab после
   создания триггера.
1. Создайте ресурсные записи для поддоментов вашего домена staging.gitlab,
   production.gitlab, prometheus.production.gitlab, grafana.production.gitlab,
   alertmanager.production.gitlab.
1. Устновка завершена, после успешного исполнения можно открывать ссылки,
   соответствующие ресурсным записям, добавленным на прошлом шаге.

Пайплайны search_engine_crawler и search_engine_ui собирают Docker-образы и
сохраняют их в GitLab image registry. Успешная сборка так же инициирует
пайплайн search_engine_deploy, который автоматически деплоит в среду staging,
после чего вручную в среду production.
